﻿using Newtonsoft.Json;

namespace FileFinder.Settings
{
    internal class SettingsContainer
    {
        [JsonProperty("start_directory")]
        public string StartDirectory { get; set; }

        [JsonProperty("search_pattern")]
        public string FileSearchPattern { get; set; }

        [JsonProperty("data")]
        public string FileData { get; set; }
    }
}
