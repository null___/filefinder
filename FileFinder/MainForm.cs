﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using FileFinder.CustomNodes;
using FileFinder.Misc;
using FileFinder.Settings;
using Newtonsoft.Json;

namespace FileFinder
{
    public partial class MainForm : Form
    {
        private readonly BackgroundWorker _backgroundWorker;
        private SearchStatus _searchStatus = SearchStatus.Stopped;

        private int _lastSearchFilesProcessed;
        private DateTime _lastSearchStartTime = DateTime.MinValue;
        private string _lastSearchLastFilePath = string.Empty;

        private const string ConfigFileName = "ffcfg.json";

        public MainForm()
        {
            InitializeComponent();

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += BackgroundWorkerSearchThread;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!File.Exists(ConfigFileName))
            {
                startDirectoryTextbox.Text = Environment.GetFolderPath(
                    Environment.SpecialFolder.Desktop);

                filePatternTextbox.Text = "*.*";

                return;
            }

            try
            {
                var contents = File.ReadAllText(ConfigFileName);
                var scont = JsonConvert.DeserializeObject<SettingsContainer>(contents);

                startDirectoryTextbox.Text = scont.StartDirectory;
                filePatternTextbox.Text = scont.FileSearchPattern;
                fileTextTextbox.Text = scont.FileData;
            } catch { /* ignored, will be automaticly replaced */ }
        }

        #region File search related

        private void BackgroundWorkerSearchThread(object sender, DoWorkEventArgs e)
        {
            Invoke((Action) (() =>
            {
                this.EnableElements(pauseButton, stopButton);

                this.DisableElements(startSearchButton, selectStartDirectoryButton,
                    startDirectoryTextbox, filePatternTextbox, fileTextTextbox);
            }));
            
            _searchStatus = SearchStatus.Running;
            _lastSearchStartTime = DateTime.Now;
            _lastSearchFilesProcessed = 0;

            try
            {
                SearchFiles();
            } catch { /* TODO: ignored for now? */ }

            _searchStatus = SearchStatus.Stopped;

            Invoke((Action) (() =>
            {
                this.EnableElements(startSearchButton, selectStartDirectoryButton,
                    startDirectoryTextbox, filePatternTextbox, fileTextTextbox);

                this.DisableElements(pauseButton, stopButton);
            }));
        }

        private void SearchFiles()
        {
            var startDirectory = startDirectoryTextbox.Text.Trim();
            var filePattern = filePatternTextbox.Text;
            var fileText = fileTextTextbox.Text;
            var needToValidateContents = !string.IsNullOrEmpty(fileText);

            var rootNode = new TreeNode("Результаты поиска");

            Invoke((Action) (() =>
            {
                searchResultsTreeView.Nodes.Clear();
                searchResultsTreeView.Nodes.Add(rootNode);
                //rootNode.ExpandAll();
            }));

            ProcessFolder(startDirectory, BuildPattern(filePattern), needToValidateContents, fileText, rootNode);
            
            Invoke((Action) (() => rootNode.Expand()));
        }

        private Regex BuildPattern(string origPattern)
        {
            return new Regex(origPattern.Replace("*", @".{0,}"));
        }

        private void PauseInterruption()
        {
            while (_searchStatus == SearchStatus.Paused)
            {
                Thread.Sleep(400);
            }
        }

        private static void IsFolderNodeHasAnyFiles(TreeNode dtn, ref bool hasAnyFiles)
        {
            foreach (var node in dtn.Nodes)
            {
                if (hasAnyFiles)
                {
                    return;
                }

                switch (node)
                {
                    case DirectoryTreeNode rdtn:
                        IsFolderNodeHasAnyFiles(rdtn, ref hasAnyFiles);
                        break;
                    case FileTreeNode _:
                        hasAnyFiles = true;
                        return;
                }
            }
        }

        private void ProcessFolder(string folder, Regex filePattern,
            bool needToValidateContents, string fileText, TreeNode selfNode)
        {
            string[] directories;

            try
            {
                directories = Directory.GetDirectories(folder);
            }
            catch
            {
                directories = Array.Empty<string>();
            }

            foreach (var directory in directories)
            {
                /*
                 * Ннеобходимо дать GUI хоть немного процессорного времени для отрисовки
                 */
                if (needToValidateContents)
                {
                    Thread.Sleep(1);
                }
                /*
                 * Задержки в отрисовке GUI замечаются во время поиска строки по файлу
                 */

                if (_searchStatus == SearchStatus.Stopped)
                {
                    return;
                }

                PauseInterruption();

                var directoryNode = DirectoryTreeNode.FromPath(directory);

                selfNode.Nodes.InvokeAdd(this, directoryNode);

                ProcessFolder(directory, filePattern, needToValidateContents,
                    fileText, directoryNode);

                var hasAnyFiles = false;

                IsFolderNodeHasAnyFiles(directoryNode, ref hasAnyFiles);

                if (!hasAnyFiles)
                {
                    selfNode.Nodes.InvokeRemove(this, directoryNode);
                }

                //directoryNode.InvokeExpandAll(this);
            }

            string[] files;

            try
            {
                files = Directory.GetFiles(folder, "*", SearchOption.TopDirectoryOnly);
            }
            catch
            {
                files = Array.Empty<string>(); // что бы можно было добавить обработку ошибки.
            }

            for (var i = 0; i < files.Length; i++)
            {
                /*
                 * Ннеобходимо дать GUI хоть немного процессорного времени для отрисовки
                 */
                if (needToValidateContents && i % 3 == 0)
                {
                    Thread.Sleep(1);
                }
                /*
                 * Задержки в отрисовке GUI замечаются во время поиска строки по файлу
                 */

                var filePath = files[i];
                Thread.Sleep(1);

                if (_searchStatus == SearchStatus.Stopped)
                {
                    return;
                }

                PauseInterruption();

                _lastSearchFilesProcessed++;
                _lastSearchLastFilePath = filePath;

                var fname = Path.GetFileName(filePath);

                if (fname == null)
                {
                    continue;
                }

                if (!filePattern.IsMatch(fname))
                {
                    continue;
                }

                if (needToValidateContents)
                {
                    try
                    {
                        var fileContents = File.ReadAllText(filePath);

                        if (!fileContents.Contains(fileText))
                        {
                            continue;
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                var fnode = FileTreeNode.FromPath(filePath);
                selfNode.Nodes.InvokeAdd(this, fnode);

                //selfNode.InvokeExpandAll(this);
                //fnode.InvokeExpandAll(this);
            }
        }

        private bool ValidateInputData()
        {
            var startDirectory = startDirectoryTextbox.Text.Trim();
            var filePattern = filePatternTextbox.Text;

            if (string.IsNullOrEmpty(startDirectory) ||
                !Directory.Exists(startDirectory))
            {
                MessageBox.Show(this,
                    "Путь до стартовой директории неверно указан.",
                    "Ошибка",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                return false;
            }

            var invalidPathChars = Path.GetInvalidPathChars()
                .Concat(Path.GetInvalidFileNameChars())
                .Distinct()
                .ToList();

            invalidPathChars.Remove('*');

            if (string.IsNullOrEmpty(filePattern) ||
                filePattern.Any(x => invalidPathChars.Contains(x)))
            {
                var r = MessageBox.Show(this,
                    "Шаблон для поиска пустой или содержит запрещенные символы.\n\n" +
                    "Нажмите \"Да\" для автоматической замены шаблона на значение по умолчанию: *.*\n\n" +
                    "(Этот шаблон выберет ВСЕ файлы без разбора)",
                    "Ошибка",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Error);

                if (r == DialogResult.Yes)
                {
                    filePatternTextbox.Text = "*.*";
                    return true;
                }

                return false;
            }

            return true;
        }

        #endregion
        
        #region UI events

        private void selectStartDirectoryButton_Click(object sender, EventArgs e)
        {
            var folderSelector = new FolderBrowserDialog
            {
                Description = "Выберите стартовую директорию"
            };

            if (folderSelector.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            startDirectoryTextbox.Text = folderSelector.SelectedPath;
        }
        
        private void filePatternHelpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this,
                "Для установки шаблона можно использовать звездочки в качестве заменителя неизвестных частей названия.\n\n" +
                "Примеры:\n" +
                "*.* - найдет все файлы\n" +
                "*.jpg - найдет все файлы с расширением JPG\n" +
                "vector_*.png - найдет все файлы, начинающиеся на vector_ и заканчивающиеся на .png\n\n" +
                "Можно использовать несколько звездочек в одном шаблоне.",
                "Информация",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void startSearchButton_Click(object sender, EventArgs e)
        {
            if(!ValidateInputData()) return;
            if(_backgroundWorker.IsBusy) return;
            
            _backgroundWorker.RunWorkerAsync();
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            switch (_searchStatus)
            {
                case SearchStatus.Running:
                    _searchStatus = SearchStatus.Paused;
                    return;
                case SearchStatus.Paused:
                    _searchStatus = SearchStatus.Running;
                    return;
            }
        }
        
        private void stopButton_Click(object sender, EventArgs e)
        {
            _searchStatus = SearchStatus.Stopped;
        }

        #endregion

        private void statusTimer_Tick(object sender, EventArgs e)
        {
            var status = string.Empty;

            if (_searchStatus == SearchStatus.Stopped)
            {
                status = "Поиск не идет.";

                if (_lastSearchFilesProcessed > 0)
                {
                    status += $"\nОбработано файлов: {_lastSearchFilesProcessed}\n";
                }
            }

            if (_searchStatus == SearchStatus.Running)
            {
                status = "Идет поиск..\n" +
                         "Обработка файла:\n" +
                         $"{_lastSearchLastFilePath}\n" +
                         $"Обработано файлов: {_lastSearchFilesProcessed}\n" +
                         $"Прошло времени: {(DateTime.Now - _lastSearchStartTime).TotalSeconds:F0} сек.";
            }

            if (_searchStatus == SearchStatus.Paused)
            {
                status = "Поиск приостановлен.";
            }

            Invoke((Action) (() => statusLabel.Text = status));
        }

        private void saveSettingsTimer_Tick(object sender, EventArgs e)
        {
            File.WriteAllText(ConfigFileName,
                JsonConvert.SerializeObject(new SettingsContainer
                {
                    StartDirectory = startDirectoryTextbox.Text,
                    FileSearchPattern = filePatternTextbox.Text,
                    FileData = fileTextTextbox.Text
                }, Formatting.Indented));
        }
    }
}
