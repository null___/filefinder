﻿using System.ComponentModel;
using System.Windows.Forms;
using FileFinder.Misc;

namespace FileFinder
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startDirectoryTextbox = new System.Windows.Forms.TextBox();
            this.startDirectoryLabel = new System.Windows.Forms.Label();
            this.selectStartDirectoryButton = new System.Windows.Forms.Button();
            this.filePatternHelpButton = new System.Windows.Forms.Button();
            this.filePatternLabel = new System.Windows.Forms.Label();
            this.filePatternTextbox = new System.Windows.Forms.TextBox();
            this.fileTextLabel = new System.Windows.Forms.Label();
            this.fileTextTextbox = new System.Windows.Forms.TextBox();
            this.startSearchButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.statusTimer = new System.Windows.Forms.Timer(this.components);
            this.saveSettingsTimer = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.searchResultsTreeView = new FileFinder.Misc.BufferedTreeView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // startDirectoryTextbox
            // 
            this.startDirectoryTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.startDirectoryTextbox.Location = new System.Drawing.Point(13, 46);
            this.startDirectoryTextbox.Margin = new System.Windows.Forms.Padding(4);
            this.startDirectoryTextbox.Name = "startDirectoryTextbox";
            this.startDirectoryTextbox.Size = new System.Drawing.Size(412, 25);
            this.startDirectoryTextbox.TabIndex = 0;
            // 
            // startDirectoryLabel
            // 
            this.startDirectoryLabel.AutoSize = true;
            this.startDirectoryLabel.Location = new System.Drawing.Point(10, 15);
            this.startDirectoryLabel.Name = "startDirectoryLabel";
            this.startDirectoryLabel.Size = new System.Drawing.Size(176, 18);
            this.startDirectoryLabel.TabIndex = 2;
            this.startDirectoryLabel.Text = "Стартовая директория:";
            // 
            // selectStartDirectoryButton
            // 
            this.selectStartDirectoryButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.selectStartDirectoryButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.selectStartDirectoryButton.Location = new System.Drawing.Point(432, 46);
            this.selectStartDirectoryButton.Name = "selectStartDirectoryButton";
            this.selectStartDirectoryButton.Size = new System.Drawing.Size(35, 25);
            this.selectStartDirectoryButton.TabIndex = 3;
            this.selectStartDirectoryButton.Text = "...";
            this.selectStartDirectoryButton.UseVisualStyleBackColor = true;
            this.selectStartDirectoryButton.Click += new System.EventHandler(this.selectStartDirectoryButton_Click);
            // 
            // filePatternHelpButton
            // 
            this.filePatternHelpButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.filePatternHelpButton.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.filePatternHelpButton.Location = new System.Drawing.Point(432, 115);
            this.filePatternHelpButton.Name = "filePatternHelpButton";
            this.filePatternHelpButton.Size = new System.Drawing.Size(35, 25);
            this.filePatternHelpButton.TabIndex = 6;
            this.filePatternHelpButton.Text = "?";
            this.filePatternHelpButton.UseVisualStyleBackColor = true;
            this.filePatternHelpButton.Click += new System.EventHandler(this.filePatternHelpButton_Click);
            // 
            // filePatternLabel
            // 
            this.filePatternLabel.AutoSize = true;
            this.filePatternLabel.Location = new System.Drawing.Point(10, 84);
            this.filePatternLabel.Name = "filePatternLabel";
            this.filePatternLabel.Size = new System.Drawing.Size(160, 18);
            this.filePatternLabel.TabIndex = 5;
            this.filePatternLabel.Text = "Шаблон имени файла:";
            // 
            // filePatternTextbox
            // 
            this.filePatternTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filePatternTextbox.Location = new System.Drawing.Point(13, 115);
            this.filePatternTextbox.Margin = new System.Windows.Forms.Padding(4);
            this.filePatternTextbox.Name = "filePatternTextbox";
            this.filePatternTextbox.Size = new System.Drawing.Size(412, 25);
            this.filePatternTextbox.TabIndex = 4;
            this.filePatternTextbox.Text = "*.*";
            // 
            // fileTextLabel
            // 
            this.fileTextLabel.AutoSize = true;
            this.fileTextLabel.Location = new System.Drawing.Point(10, 153);
            this.fileTextLabel.Name = "fileTextLabel";
            this.fileTextLabel.Size = new System.Drawing.Size(304, 18);
            this.fileTextLabel.TabIndex = 7;
            this.fileTextLabel.Text = "Текст, который должен содержать файл:";
            // 
            // fileTextTextbox
            // 
            this.fileTextTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileTextTextbox.Location = new System.Drawing.Point(13, 183);
            this.fileTextTextbox.Multiline = true;
            this.fileTextTextbox.Name = "fileTextTextbox";
            this.fileTextTextbox.Size = new System.Drawing.Size(451, 108);
            this.fileTextTextbox.TabIndex = 8;
            // 
            // startSearchButton
            // 
            this.startSearchButton.Location = new System.Drawing.Point(13, 297);
            this.startSearchButton.Name = "startSearchButton";
            this.startSearchButton.Size = new System.Drawing.Size(107, 30);
            this.startSearchButton.TabIndex = 10;
            this.startSearchButton.Text = "Начать";
            this.startSearchButton.UseVisualStyleBackColor = true;
            this.startSearchButton.Click += new System.EventHandler(this.startSearchButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(335, 297);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(132, 30);
            this.stopButton.TabIndex = 12;
            this.stopButton.Text = "Остановить";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Enabled = false;
            this.pauseButton.Location = new System.Drawing.Point(126, 297);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(203, 30);
            this.pauseButton.TabIndex = 13;
            this.pauseButton.Text = "Поставить/убрать паузу";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusLabel.Location = new System.Drawing.Point(10, 341);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(25, 13);
            this.statusLabel.TabIndex = 14;
            this.statusLabel.Text = "...";
            // 
            // statusTimer
            // 
            this.statusTimer.Enabled = true;
            this.statusTimer.Interval = 300;
            this.statusTimer.Tick += new System.EventHandler(this.statusTimer_Tick);
            // 
            // saveSettingsTimer
            // 
            this.saveSettingsTimer.Enabled = true;
            this.saveSettingsTimer.Interval = 1000;
            this.saveSettingsTimer.Tick += new System.EventHandler(this.saveSettingsTimer_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.searchResultsTreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.startDirectoryLabel);
            this.splitContainer1.Panel2.Controls.Add(this.statusLabel);
            this.splitContainer1.Panel2.Controls.Add(this.startDirectoryTextbox);
            this.splitContainer1.Panel2.Controls.Add(this.pauseButton);
            this.splitContainer1.Panel2.Controls.Add(this.selectStartDirectoryButton);
            this.splitContainer1.Panel2.Controls.Add(this.stopButton);
            this.splitContainer1.Panel2.Controls.Add(this.filePatternTextbox);
            this.splitContainer1.Panel2.Controls.Add(this.startSearchButton);
            this.splitContainer1.Panel2.Controls.Add(this.filePatternLabel);
            this.splitContainer1.Panel2.Controls.Add(this.fileTextTextbox);
            this.splitContainer1.Panel2.Controls.Add(this.filePatternHelpButton);
            this.splitContainer1.Panel2.Controls.Add(this.fileTextLabel);
            this.splitContainer1.Size = new System.Drawing.Size(719, 491);
            this.splitContainer1.SplitterDistance = 239;
            this.splitContainer1.TabIndex = 15;
            // 
            // searchResultsTreeView
            // 
            this.searchResultsTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchResultsTreeView.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.searchResultsTreeView.Location = new System.Drawing.Point(0, 0);
            this.searchResultsTreeView.Name = "searchResultsTreeView";
            this.searchResultsTreeView.Size = new System.Drawing.Size(239, 491);
            this.searchResultsTreeView.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(719, 491);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(735, 530);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поиск файлов";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TextBox startDirectoryTextbox;
        private Label startDirectoryLabel;
        private Button selectStartDirectoryButton;
        private Button filePatternHelpButton;
        private Label filePatternLabel;
        private TextBox filePatternTextbox;
        private Label fileTextLabel;
        private TextBox fileTextTextbox;
        private BufferedTreeView searchResultsTreeView;
        private Button startSearchButton;
        private Button stopButton;
        private Button pauseButton;
        private Label statusLabel;
        private Timer statusTimer;
        private Timer saveSettingsTimer;
        private SplitContainer splitContainer1;
    }
}

