﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace FileFinder.Misc
{
    internal class BufferedTreeView : TreeView {
        private const int TvmSetExtendedStyle = 0x1100 + 44;
        private const int TvsExDoubleBuffer = 0x0004;

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        protected override void OnHandleCreated(EventArgs e) {
            SendMessage(Handle,
                TvmSetExtendedStyle,
                (IntPtr) TvsExDoubleBuffer,
                (IntPtr) TvsExDoubleBuffer);

            base.OnHandleCreated(e);
        }
    }
}