﻿namespace FileFinder.Misc
{
    public enum SearchStatus
    {
        Running,
        Paused,
        Stopped
    }
}
