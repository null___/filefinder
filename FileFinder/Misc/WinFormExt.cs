﻿using System.Windows.Forms;

namespace FileFinder.Misc
{
    public static class WinFormExt
    {
        public static void EnableElements(this Form f, params Control[] elements)
        {
            foreach (var element in elements)
            {
                element.Enabled = true;
            }
        }

        public static void DisableElements(this Form f, params Control[] elements)
        {
            foreach (var element in elements)
            {
                element.Enabled = false;
            }
        }
    }
}
