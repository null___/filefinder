﻿using System;
using System.Windows.Forms;

namespace FileFinder.CustomNodes
{
    public static class TreeNodesExt
    {
        public static void InvokeExpandAll(this TreeNode tn, Control c)
        {
            c.Invoke((Action) tn.ExpandAll);
        }

        public static void InvokeAdd(this TreeNodeCollection tnc, Control c,
            TreeNode node)
        {
            c.Invoke((Action) (() => tnc.Add(node)));
        }

        public static void InvokeRemove(this TreeNodeCollection tnc, Control c,
            TreeNode node)
        {
            c.Invoke((Action) (() => tnc.Remove(node)));
        }
    }
}
