﻿using System.IO;
using System.Windows.Forms;

namespace FileFinder.CustomNodes
{
    internal class DirectoryTreeNode : TreeNode
    {
        public string FullFilePath { get; set; }

        public DirectoryTreeNode(string fullFilePath)
            : base(Path.GetFileName(fullFilePath))
        {
            FullFilePath = fullFilePath;
        }

        public static DirectoryTreeNode FromPath(string path)
        {
            return new DirectoryTreeNode(path);
        }
    }
}
