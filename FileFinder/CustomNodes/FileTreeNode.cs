﻿using System.IO;
using System.Windows.Forms;

namespace FileFinder.CustomNodes
{
    internal class FileTreeNode : TreeNode
    {
        public string FullFilePath { get; set; }

        public FileTreeNode(string fullFilePath)
            : base(Path.GetFileName(fullFilePath))
        {
            FullFilePath = fullFilePath;
        }

        public static FileTreeNode FromPath(string path)
        {
            return new FileTreeNode(path);
        }
    }
}
